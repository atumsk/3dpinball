﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumperSCR : MonoBehaviour {
    public float bumperForce = 100f;
    public float bForceRadius = 1f;

    void OnCollisionEnter()
    {
        foreach(Collider col in Physics.OverlapSphere(transform.position, bForceRadius))
        {
            if (col.GetComponent<Rigidbody>())
            {
                col.GetComponent<Rigidbody>().AddExplosionForce(bumperForce, transform.position, bForceRadius);
            }
        }
    }

}