﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnBallSCR : MonoBehaviour
{

    public LevelManagerSCR levelManager;

    // Use this for initialization
    void Start()
    {
        levelManager = FindObjectOfType<LevelManagerSCR>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")
        {
           levelManager.RespawnBall();
        }
    }
}