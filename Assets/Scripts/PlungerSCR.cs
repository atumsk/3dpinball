﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlungerSCR : MonoBehaviour
{
    float force;
    float minForce = 0f;
    public float maxForce = 100f;
    public Slider forceSlider;
    List<Rigidbody> ballList;
    bool ballReady;

    void Start()
    {
        forceSlider.minValue = 0f;
        forceSlider.maxValue = maxForce;
        ballList = new List<Rigidbody>();
    }

    void Update()
    {
        if (ballReady)
        {
            forceSlider.gameObject.SetActive(true);
        }
        else
        {
            forceSlider.gameObject.SetActive(false);
        }

        forceSlider.value = force;
        if (ballList.Count > 0)
        {
            ballReady = true;
            if (Input.GetKey(KeyCode.Space))
            {
                if (force <= maxForce)
                {
                    force += 50 * Time.deltaTime;
                }
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                foreach (Rigidbody r in ballList)
                {
                    r.AddForce(force * Vector3.forward);
                }
            }
        }
        else
        {
            ballReady = false;
            force = 0f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            ballList.Add(other.gameObject.GetComponent<Rigidbody>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Ball"))
        {
            ballList.Remove(other.gameObject.GetComponent<Rigidbody>());
            force = 0f;
        }
    }
}