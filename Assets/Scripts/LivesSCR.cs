﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LivesSCR : MonoBehaviour
{
    public int startingLives;
    public int lifeCounter;

    private Text livesText;

    // Use this for initialization
    void Start()
    {
        livesText = GetComponent<Text>();
        lifeCounter = startingLives;
    }

    // Update is called once per frame
    void Update()
    {
        livesText.text = "LIVES - " + lifeCounter;
    }

    public void TakeLife()
    {
        lifeCounter--;
    }

    public void Death()
    {
        if (lifeCounter < 1)
        {
            SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
        }
    }
}
