﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManagerSCR : MonoBehaviour
{

    public GameObject currentStartingPoint;

    private BallSCR ball;
    private LivesSCR lives;

    // Use this for initialization
    void Start()
    {
        ball = FindObjectOfType<BallSCR>();
        lives = FindObjectOfType<LivesSCR>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void RespawnBall()
    {
        ball.transform.position = currentStartingPoint.transform.position;
        lives.TakeLife();
    }
}